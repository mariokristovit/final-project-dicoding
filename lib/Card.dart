import 'package:flutter/material.dart';

class Timeline extends StatelessWidget {
  final String message;
  Timeline(this.message);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text(message + " Post", style: TextStyle(color: Colors.white)),
        backgroundColor: Color.fromRGBO(255, 165, 0, 1),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Color.fromRGBO(255, 155, 0, 1), Color(0xFFF56D5D)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            )),
          ),
          Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.7,
              child: ListView(children: <Widget>[
                firstCard(context),
                secondCard(context)
              ],),
            ),
          )
        ],
      ),
    );
  }

  Card firstCard(BuildContext context) {
    return Card(
              elevation: 10,
              child: Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.7,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          image: DecorationImage(
                              image: NetworkImage(
                                    "https://www.toptal.com/designers/subtlepatterns/patterns/memphis-mini.png"),
                              fit: BoxFit.cover)),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.35,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(4),
                          topRight: Radius.circular(4),
                        ),
                        image: DecorationImage(
                            image: NetworkImage(
                                  "https://static1.cbrimages.com/wordpress/wp-content/uploads/2019/12/Featured-Image-Nami-Cropped.jpg"),
                            fit: BoxFit.cover)),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        20,
                        50 + MediaQuery.of(context).size.height * 0.35,
                        20,
                        20),
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Nami inner beauty at the beach",
                            maxLines: 2,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xFFF56D5D), fontSize: 25),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 20, 0, 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Posted on ",
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 15),
                                ),
                                Text(
                                  "January 18, 2020",
                                  style: TextStyle(
                                      color: Color(0xFFF56D5D), fontSize: 15),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Spacer(flex: 10),
                              Icon(Icons.thumb_up, size: 18, color: Colors.grey,),
                              Spacer(flex: 1),
                              Text("110", style: TextStyle(color: Colors.grey),),
                              Spacer(flex: 5),
                              Icon(Icons.comment, size: 18, color: Colors.grey,),                                
                              Spacer(flex: 1),
                              Text("999", style: TextStyle(color: Colors.grey),),                               
                              Spacer(flex: 10)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
  }

  Card secondCard(BuildContext context) {
    return Card(
              elevation: 10,
              child: Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.7,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          image: DecorationImage(
                              image: NetworkImage(
                                    "https://www.toptal.com/designers/subtlepatterns/patterns/memphis-mini.png"),
                              fit: BoxFit.cover)),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.35,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(4),
                          topRight: Radius.circular(4),
                        ),
                        image: DecorationImage(
                            image: NetworkImage(
                                "https://www.alfabetajuega.com/wp-content/uploads/2020/04/one-piece-nami-sanji.jpg"),
                            fit: BoxFit.cover)),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        20,
                        50 + MediaQuery.of(context).size.height * 0.35,
                        20,
                        20),
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Sanji fallin sleep.",
                            maxLines: 2,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xFFF56D5D), fontSize: 25),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 20, 0, 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Posted on ",
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 15),
                                ),
                                Text(
                                  "August 01, 2019",
                                  style: TextStyle(
                                      color: Color(0xFFF56D5D), fontSize: 15),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Spacer(flex: 10),
                              Icon(Icons.thumb_up, size: 18, color: Colors.grey,),
                              Spacer(flex: 1),
                              Text("101", style: TextStyle(color: Colors.grey),),
                              Spacer(flex: 5),
                              Icon(Icons.comment, size: 18, color: Colors.grey,),                                
                              Spacer(flex: 1),
                              Text("22", style: TextStyle(color: Colors.grey),),                               
                              Spacer(flex: 10)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
  }
}
