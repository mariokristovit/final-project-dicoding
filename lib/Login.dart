import 'package:final_project_dicoding/Card.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController controller = TextEditingController();
  String message = 'Annisa';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.deepOrange,
      home: Scaffold(
          backgroundColor: Color.fromRGBO(255, 165, 0, 1),
          body: Container(
            margin: EdgeInsets.only(top: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Nakama Portal",
                  style: TextStyle(fontWeight: FontWeight.w800, fontSize: 40,color: Colors.white),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(20),
                      child: TextField(
                        decoration: InputDecoration(
                            fillColor: Colors.yellow[50],
                            filled: true,
                            // prefix: Container(width: 5, height: 5, color: Colors.red,),
                            prefixIcon: Icon(Icons.person),
                            prefixStyle: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20)),
                            labelText: "Username",
                            hintStyle: TextStyle(fontSize: 12)),
                        onChanged: (value) {
                          setState(() {});
                        },
                        controller: controller,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      child: TextField(
                        decoration: InputDecoration(
                            fillColor: Colors.yellow[50],
                            filled: true,
                            // prefix: Container(width: 5, height: 5, color: Colors.red,),
                            prefixIcon: Icon(Icons.lock),
                            prefixStyle: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w600),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20)),
                            labelText: "Password",
                            hintStyle: TextStyle(fontSize: 12)),
                        maxLength: 10,
                        obscureText: true,
                        onChanged: (value) {
                          setState(() {});
                        },
                      ),
                    ),
                  ],
                ),
                Material(
                  borderRadius: BorderRadius.circular(20),
                  elevation: 2,
                  child: Container(
                    width: 150,
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                            colors: [Colors.red, Colors.orangeAccent],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter)),
                    child: Material(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Colors.amber,
                        borderRadius: BorderRadius.circular(20),
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return Timeline(controller.text);
                          }));
                        },
                        child: Center(
                            child: Text(
                          "Login",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        )),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
